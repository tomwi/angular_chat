import { פרויקטאנגולרPage } from './app.po';

describe('פרויקט-אנגולר App', function() {
  let page: פרויקטאנגולרPage;

  beforeEach(() => {
    page = new פרויקטאנגולרPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
