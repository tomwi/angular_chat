

import "dart:html";
import 'package:angular2/core.dart';
import 'package:angular2_components/angular2_components.dart';

import './app_header/app_header.dart';
import 'firebase_service.dart';

import 'scroll_down.dart';

@Component(
  selector: 'my-app',
  styleUrls: const ['app_component.css'],
  templateUrl: 'app_component.html',
  directives: const [AppHeader, VuScrollDown],
  providers: const [materialProviders, FirebaseService],
)
class AppComponent {
  final FirebaseService fbService;

  bool isGroupActive;

//  Logged User
  String newGroupName = "";
  String activeGroup = "";
  String chatMessage = "";
  String userToInvite = "";


  AppComponent(FirebaseService this.fbService) {
    this.isGroupActive = false;
  }

  /**
   * Function for adding new group
   */
  void addGroup() {
    String groupName = newGroupName.trim();
    if (groupName.isNotEmpty) {
      fbService.addGroup(groupName);
      newGroupName = "";
    }
  }

  /**
   * Function is active when group is selected
   */
  void selectGroup(group) {
    isGroupActive = true;
    activeGroup = group.name;
    fbService.selectGroup(activeGroup);
  }

  /**
   * Function is active when a massage is sent
   */
  void sendMessage() {
    String chatMsg = chatMessage.trim();
    if (chatMsg.isNotEmpty) {
      fbService.sendMessage(text: chatMsg, groupName: activeGroup, imageURL: null);
      chatMessage = "";
    }
  }


  /**
   * Fcuntion for add member
   */
  void addMember() {
    String newMemb = userToInvite.trim();
    if (newMemb.isNotEmpty) {
      fbService.addMember(newMemb, activeGroup);
      userToInvite = "";
    }
  }

  /**
   * Function for removing a member
   */
  void removeMember(memberName) {
    fbService.removeMember(memberName, activeGroup);
  }
  /**
	* Authontication
	*/	
  isYou(email) {
    if (fbService.user.displayName != email) {
      return true;
    } else {
      return false;
    }
  }
  /**
	* Authontication 
	*/
  isMe(email) {
    if (fbService.user.displayName == email) {
      return true;
    } else {
      return false;
    }
  }
  /**
	* Function for send massage
	*/
  void sendImageMessage(FileList files) {
    if (files.isNotEmpty) {
      fbService.sendImage(files.first, activeGroup);
    }
  }
}
